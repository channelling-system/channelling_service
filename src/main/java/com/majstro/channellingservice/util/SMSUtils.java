package com.majstro.channellingservice.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.springframework.http.HttpHeaders;

public class SMSUtils {
	private SMSUtils() {
		throw new IllegalStateException("Utility class");
	}

	public static HttpHeaders prepareSMSHeaders(String smsUsername, String smsPw) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("USER", smsUsername);
		headers.set("DIGEST", smsPw);
		headers.set("CREATED",
				LocalDateTime.now(ZoneId.of("Asia/Kolkata")).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		return headers;
	}
}
