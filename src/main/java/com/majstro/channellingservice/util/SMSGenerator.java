package com.majstro.channellingservice.util;

import com.majstro.channellingservice.dto.BulkSMSDTO;
import com.majstro.channellingservice.dto.SMSDTO;
import com.majstro.channellingservice.model.Appoinment;
import com.majstro.channellingservice.model.SMSTypes;
import com.majstro.channellingservice.model.Session;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class SMSGenerator {

    @Value("${sms.auth.mask}")
    private String mask;

    @Value("${sms.auth.campaignname}")
    private String campaignName;

    @Value("${sms.message.confirmedAppointment}")
    private String confirmedAppointmentMsg;

    @Value("${sms.message.notConfirmedAppointment}")
    private String notConfirmedAppointmentMsg;

    @Value("${sms.message.sessionDelayed}")
    private String sessionDelayedMsg;

    @Value("${sms.message.sessionCanceled}")
    private String sessionCanceledMsg;

    @Value("${sms.message.sessionStarted}")
    private String sessionStartedMsg;

    public BulkSMSDTO generateSMS(Session session, SMSTypes type) {

        BulkSMSDTO bulkSMSDTO = null;

        switch (type) {
            case STARTED:
                bulkSMSDTO = generateSessionStartedSMS(session);
                break;
            case DELAYED:
                bulkSMSDTO = generateSessionDelayedSMS(session);
                break;
            case CANCELLED:
                bulkSMSDTO = generateSessionCancelledSMS(session);
                break;
            default:
                break;
        }
        return bulkSMSDTO;
    }

    public BulkSMSDTO generateSMS(Appoinment appoinment, SMSTypes type) {

        BulkSMSDTO bulkSMSDTO = null;

        switch (type) {
            case CONFIRMED:
                bulkSMSDTO = generateConfirmedAppointmentSMS(appoinment);
                break;
            case NOT_CONFIRMED:
                bulkSMSDTO = generateNotConfirmedAppointmentSMS(appoinment);
                break;
            default:
                break;
        }
        return bulkSMSDTO;
    }


    private BulkSMSDTO generateSessionStartedSMS(Session session) {

        List<SMSDTO> smsdtoList = session.getAppoinmentList().stream().map(a -> {
            return new SMSDTO("94" + a.getPatient().getTelephone().substring(1), mask, campaignName,
                    MessageFormat.format(sessionStartedMsg, a.getNumber(), session.getDoctor().getName(),
                            session.getDate().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)), session.getRoom().getName()));
        }).collect(Collectors.toList());

        return new BulkSMSDTO(smsdtoList);
    }

    private BulkSMSDTO generateSessionDelayedSMS(Session session) {

        Map<Long, String> delayLabels = new HashMap<>();
        delayLabels.put(30L, "30 minutes");
        delayLabels.put(60L, "1 hour");
        delayLabels.put(90L, "1 hour and 30 minutes");
        delayLabels.put(120L, "2 hours");

        List<SMSDTO> smsdtoList = session.getAppoinmentList().stream().map(a -> {
            return new SMSDTO("94" + a.getPatient().getTelephone().substring(1), mask, campaignName,
                    MessageFormat.format(sessionDelayedMsg, a.getNumber(), session.getDoctor().getName(),
                            delayLabels.get(session.getDelayInMinutes())));
        }).collect(Collectors.toList());

        return new BulkSMSDTO(smsdtoList);
    }

    private BulkSMSDTO generateSessionCancelledSMS(Session session) {

        List<SMSDTO> smsdtoList = session.getAppoinmentList().stream().map(a -> {
            return new SMSDTO("94" + a.getPatient().getTelephone().substring(1), mask, campaignName,
                    MessageFormat.format(sessionCanceledMsg, a.getNumber(), session.getDoctor().getName(),
                            session.getDate().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL))));
        }).collect(Collectors.toList());

        return new BulkSMSDTO(smsdtoList);
    }

    private BulkSMSDTO generateConfirmedAppointmentSMS(Appoinment appoinment) {
        Session session = appoinment.getSession();

        double fee = appoinment.getDoctorFee() + appoinment.getHospitalFee();

        String text = MessageFormat.format(confirmedAppointmentMsg, appoinment.getNumber(),
                session.getDoctor().getName(),
                session.getDate().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)), formatTime(appoinment),
                session.getRoom().getName(), fee);

        SMSDTO sms = new SMSDTO("94" + appoinment.getPatient().getTelephone().substring(1), mask, campaignName, text);
        return new BulkSMSDTO(List.of(sms));
    }

    private BulkSMSDTO generateNotConfirmedAppointmentSMS(Appoinment appoinment) {

        Session session = appoinment.getSession();

        double fee = appoinment.getDoctorFee() + appoinment.getHospitalFee();

        String text = MessageFormat.format(notConfirmedAppointmentMsg, appoinment.getNumber(),
                session.getDoctor().getName(),
                session.getDate().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)), formatTime(appoinment),
                session.getRoom().getName(), fee);

        SMSDTO sms = new SMSDTO("94" + appoinment.getPatient().getTelephone().substring(1), mask, campaignName, text);
        return new BulkSMSDTO(List.of(sms));
    }

    private String formatTime(Appoinment appoinment) {
        Session session = appoinment.getSession();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime time = session.getStart()
                .plusMinutes(session.getDoctor().getConsultationTime() * (appoinment.getNumber() - 1));
        return time.format(dtf);
    }
}
