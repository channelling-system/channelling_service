package com.majstro.channellingservice.auth.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class AuthenticationRequest {
    private final String username;
    private final String password;
}
