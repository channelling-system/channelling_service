package com.majstro.channellingservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.majstro.channellingservice.model.Appoinment;

import java.util.List;

@Repository
public interface AppoinmentRepository extends JpaRepository<Appoinment, Long> {

    List<Appoinment> findAllAssessmentsBySessionId(long sessionId);

}
