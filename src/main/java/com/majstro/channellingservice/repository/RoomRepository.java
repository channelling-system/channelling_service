package com.majstro.channellingservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.majstro.channellingservice.model.Room;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {

}
