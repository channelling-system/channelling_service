package com.majstro.channellingservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.majstro.channellingservice.model.Patient;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {

}
