package com.majstro.channellingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class ChanellingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChanellingServiceApplication.class, args);
	}
}
