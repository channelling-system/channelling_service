package com.majstro.channellingservice.service;

import java.util.List;

import com.majstro.channellingservice.dto.input.AppoinmentInputDTO;
import com.majstro.channellingservice.dto.output.AppoinmentOutputDTO;

public interface AppoinmentService {

	AppoinmentOutputDTO save(AppoinmentInputDTO doctorInput);
	
	AppoinmentOutputDTO update(Long appoinmentId,AppoinmentInputDTO appoinmentInput);

	void deleteById(Long id);

	AppoinmentOutputDTO findById(Long id);

	List<AppoinmentOutputDTO> findAll();

	List<AppoinmentOutputDTO> findAllBySessionId(long sessionId);

}
