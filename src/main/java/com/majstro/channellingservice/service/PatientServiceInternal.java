package com.majstro.channellingservice.service;

import com.majstro.channellingservice.dto.input.patientInputDTO;
import com.majstro.channellingservice.model.Patient;

public interface PatientServiceInternal {

	Patient findEntityById(Long id);
	Patient saveAndGetEntity( patientInputDTO patientInput);
	

}
