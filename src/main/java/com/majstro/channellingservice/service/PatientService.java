package com.majstro.channellingservice.service;

import java.util.List;

import com.majstro.channellingservice.dto.input.patientInputDTO;
import com.majstro.channellingservice.dto.output.PatientOutputDTO;

public interface PatientService {

	PatientOutputDTO save( patientInputDTO patientInput);
	
	PatientOutputDTO update(Long patientId,patientInputDTO patientInput);

	void deleteById(Long id);

	PatientOutputDTO findById(Long id);

	List<PatientOutputDTO> findAll();
}
