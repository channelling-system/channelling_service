package com.majstro.channellingservice.service;

import java.util.List;

import com.majstro.channellingservice.dto.input.DoctorInputDTO;
import com.majstro.channellingservice.dto.output.DoctorOutputDTO;

public interface DoctorService {

	DoctorOutputDTO save(DoctorInputDTO doctorInput);
	
	DoctorOutputDTO update(Long doctorId,DoctorInputDTO doctorInput);

	void deleteById(Long id);

	DoctorOutputDTO findById(Long id);

	List<DoctorOutputDTO> findAll();
}
