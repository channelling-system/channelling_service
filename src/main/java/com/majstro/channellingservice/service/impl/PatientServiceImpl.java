package com.majstro.channellingservice.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.majstro.channellingservice.dto.input.patientInputDTO;
import com.majstro.channellingservice.dto.output.PatientOutputDTO;
import com.majstro.channellingservice.exception.ResourceNotFoundException;
import com.majstro.channellingservice.model.Patient;
import com.majstro.channellingservice.repository.PatientRepository;
import com.majstro.channellingservice.service.PatientService;
import com.majstro.channellingservice.service.PatientServiceInternal;

@Service
public class PatientServiceImpl implements PatientService, PatientServiceInternal {

	private PatientRepository repository;

	public PatientServiceImpl(PatientRepository repository) {
		this.repository = repository;
	}

	@Override
	public PatientOutputDTO save(patientInputDTO patientInput) {
		return repository.save(saveAndGetEntity(patientInput)).viewAsDTO();
	}

	@Override
	public Patient saveAndGetEntity(patientInputDTO patientInput) {
		return Patient.builder().id(patientInput.getId()).name(patientInput.getName())
				.address(patientInput.getAddress()).dob(patientInput.getDob()).age(patientInput.getAge())
				.gender(patientInput.getGender()).telephone(patientInput.getTelephone())
				.nic(patientInput.getNic()).build();

	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public PatientOutputDTO findById(Long id) {

		return repository.findById(id).map(p -> {
			return p.viewAsDTO();
		}).orElseThrow(() -> new ResourceNotFoundException("Patient not found for id=" + id));
	}

	@Override
	public List<PatientOutputDTO> findAll() {
		return repository.findAll().stream().map(Patient::viewAsDTO).collect(Collectors.toList());
	}

	@Override
	public PatientOutputDTO update(Long patientId, patientInputDTO patientInput) {
		return repository.findById(patientId).map(p -> {
			Patient patient = Patient.builder().id(patientId).name(patientInput.getName())
					.address(patientInput.getAddress()).dob(patientInput.getDob()).age(patientInput.getAge())
					.gender(patientInput.getGender()).telephone(patientInput.getTelephone())
					.nic(patientInput.getNic()).build();
			return repository.save(patient).viewAsDTO();
		}).orElseThrow(() -> new ResourceNotFoundException("Patient not found for id=" + patientId));
	}

	@Override
	public Patient findEntityById(Long id) {
		return repository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Patient not found for id=" + id));

	}

}
