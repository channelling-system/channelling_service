package com.majstro.channellingservice.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.majstro.channellingservice.dto.BulkSMSDTO;
import com.majstro.channellingservice.service.SMSSender;
import com.majstro.channellingservice.util.SMSUtils;

@Service
public class SMSSenderImpl implements SMSSender {

	private final RestTemplate restTemplate;
	private final String smsUsername;
	private final String smsPw;
	private final String smsEndpoint;

	public SMSSenderImpl(final RestTemplateBuilder restTemplateBuilder,
			@Value("${sms.auth.username}") final String smsUsername, @Value("${sms.auth.pw}") final String smsPw,
			@Value("${sms.endpoint}") final String smsEndpoint) {
		super();
		this.restTemplate = restTemplateBuilder.build();
		this.smsUsername = smsUsername;
		this.smsPw = smsPw;
		this.smsEndpoint = smsEndpoint;
	}

	@Override
	@Async
	public void send(BulkSMSDTO smses) {
		HttpEntity<BulkSMSDTO> request = new HttpEntity<>(smses, SMSUtils.prepareSMSHeaders(smsUsername, smsPw));
		this.restTemplate.postForEntity(smsEndpoint, request, String.class);
	}

}
