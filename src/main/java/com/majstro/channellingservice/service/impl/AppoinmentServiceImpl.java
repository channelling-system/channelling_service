package com.majstro.channellingservice.service.impl;

import com.majstro.channellingservice.dto.BulkSMSDTO;
import com.majstro.channellingservice.dto.input.AppoinmentInputDTO;
import com.majstro.channellingservice.dto.output.AppoinmentOutputDTO;
import com.majstro.channellingservice.exception.ResourceNotFoundException;
import com.majstro.channellingservice.model.Appoinment;
import com.majstro.channellingservice.model.Patient;
import com.majstro.channellingservice.model.SMSTypes;
import com.majstro.channellingservice.model.Session;
import com.majstro.channellingservice.repository.AppoinmentRepository;
import com.majstro.channellingservice.service.AppoinmentService;
import com.majstro.channellingservice.service.PatientServiceInternal;
import com.majstro.channellingservice.service.SMSSender;
import com.majstro.channellingservice.service.SessionServiceInternal;
import com.majstro.channellingservice.util.SMSGenerator;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class AppoinmentServiceImpl implements AppoinmentService {

	private AppoinmentRepository repository;
	private SessionServiceInternal sessionService;
	private PatientServiceInternal patientService;
	private final SMSSender smsSender;
	private final SMSGenerator smsGenerator;

	public AppoinmentServiceImpl(AppoinmentRepository repository, SessionServiceInternal sessionService,
			PatientServiceInternal patientService, SMSSender smsSender, SMSGenerator smsGenerator) {
		this.repository = repository;
		this.sessionService = sessionService;
		this.patientService = patientService;
		this.smsSender = smsSender;
		this.smsGenerator = smsGenerator;
	}

	@Override
	public AppoinmentOutputDTO save(AppoinmentInputDTO appoinmentInput) {

		BulkSMSDTO bulkSMSDTO;

		if (Objects.isNull(appoinmentInput.getPatient())) {
			throw new IllegalArgumentException("Cannot Create an appoinment without a patient");
		}

		Patient patient = (appoinmentInput.getPatient().getId() > 0)
				? patientService.findEntityById(appoinmentInput.getPatient().getId())
				: patientService.saveAndGetEntity(appoinmentInput.getPatient());

		Session session = sessionService.findEntityById(appoinmentInput.getSessionId());
		Appoinment appoinmentToSave = Appoinment.builder().patient(patient).session(session).doctorFee(session.getDoctorFee())
				.hospitalFee(session.getHospitalFee()).confirmed(appoinmentInput.isConfirmed())
				.number(!Objects.isNull(session.getAppoinmentList()) ? session.getAppoinmentList().size() + 1 : 1)
				.build();
		appoinmentToSave = repository.save(appoinmentToSave);

		if (appoinmentToSave.isConfirmed()) {
			bulkSMSDTO = smsGenerator.generateSMS(appoinmentToSave, SMSTypes.CONFIRMED);
		} else {
			bulkSMSDTO = smsGenerator.generateSMS(appoinmentToSave, SMSTypes.NOT_CONFIRMED);
		}

		this.smsSender.send(bulkSMSDTO);

		return appoinmentToSave.viewAsDTO();
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public AppoinmentOutputDTO findById(Long id) {

		return repository.findById(id).map(p -> {
			return p.viewAsDTO();
		}).orElseThrow(() -> new ResourceNotFoundException("Appoinment not found for id=" + id));
	}

	@Override
	public List<AppoinmentOutputDTO> findAll() {
		return repository.findAll().stream().map(Appoinment::viewAsDTO).collect(Collectors.toList());
	}

	@Override
	public List<AppoinmentOutputDTO> findAllBySessionId(long sessionId) {
		return repository.findAllAssessmentsBySessionId(sessionId).stream().map(Appoinment::viewAsDTO)
				.collect(Collectors.toList());
	}

	@Override
	public AppoinmentOutputDTO update(Long appoinmentId, AppoinmentInputDTO appoinmentInput) {
		if (Objects.isNull(appoinmentInput.getPatient())) {
			throw new IllegalArgumentException("Cannot update an appoinment without a patient");
		}

		return repository.findById(appoinmentId).map(p -> {

			Patient patient = (appoinmentInput.getPatient().getId() > 0)
					? patientService.findEntityById(appoinmentInput.getPatient().getId())
					: patientService.saveAndGetEntity(appoinmentInput.getPatient());

			Session session = sessionService.findEntityById(appoinmentInput.getSessionId());
			Appoinment appoinmentToSave = Appoinment.builder().id(p.getId()).patient(patient).session(session)
					.doctorFee(session.getDoctorFee()).hospitalFee(session.getHospitalFee())
					.confirmed(appoinmentInput.isConfirmed()).number(p.getNumber()).build();

			appoinmentToSave = repository.save(appoinmentToSave);

			if (appoinmentToSave.isConfirmed()) {
				smsSender.send(smsGenerator.generateSMS(appoinmentToSave, SMSTypes.CONFIRMED));
			}

			return appoinmentToSave.viewAsDTO();
		}).orElseThrow(() -> new ResourceNotFoundException("Appoinment not found for id=" + appoinmentId));
	}
}
