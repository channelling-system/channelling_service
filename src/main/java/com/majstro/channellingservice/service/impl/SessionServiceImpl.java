package com.majstro.channellingservice.service.impl;

import com.majstro.channellingservice.dto.BulkSMSDTO;
import com.majstro.channellingservice.dto.input.SessionInputDTO;
import com.majstro.channellingservice.dto.input.SessionStatusUpdateInputDTO;
import com.majstro.channellingservice.dto.output.SessionOutputDTO;
import com.majstro.channellingservice.exception.ResourceNotFoundException;
import com.majstro.channellingservice.model.*;
import com.majstro.channellingservice.repository.SessionRepository;
import com.majstro.channellingservice.service.*;
import com.majstro.channellingservice.util.SMSGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class SessionServiceImpl implements SessionService, SessionServiceInternal {

    private SessionRepository repository;
    private DoctorServiceInternal doctorService;
    private RoomServiceInternal roomService;
    private double hospitalFee;
    private final SMSSender smsSender;
    private final SMSGenerator smsGenerator;

    public SessionServiceImpl(SessionRepository repository, DoctorServiceInternal doctorService,
                              RoomServiceInternal roomService, @Value("${majstro.channelling.hospitalFee}") double hospitalFee,
                              SMSSender smsSender, SMSGenerator smsGenerator) {
        this.repository = repository;
        this.doctorService = doctorService;
        this.roomService = roomService;
        this.hospitalFee = hospitalFee;
        this.smsSender = smsSender;
        this.smsGenerator = smsGenerator;
    }

    @Override
    public SessionOutputDTO save(SessionInputDTO sessionInput) {

        Doctor doctor = doctorService.findEntityById(sessionInput.getDoctorId());
        Room room = roomService.findEntityById(sessionInput.getRoomId());

        Session sessionToSave = Session.builder().date(sessionInput.getDate()).start(sessionInput.getStart())
                .end(sessionInput.getEnd()).status(sessionInput.getStatus()).note(sessionInput.getNote()).doctor(doctor)
                .room(room).doctorFee(doctor.getFee()).hospitalFee(hospitalFee).build();

        return repository.save(sessionToSave).viewAsDTO();
    }

    @Override
    public void deleteById(Long id) {
        // TODO Auto-generated method stub

    }

    @Override
    public SessionOutputDTO findById(Long id) {

        return repository.findById(id).map(s -> {
            return s.viewAsDTO();
        }).orElseThrow(() -> new ResourceNotFoundException("Session not found for id=" + id));
    }

    @Override
    public List<SessionOutputDTO> findAll() {
        return repository.findAll().stream().map(Session::viewAsDTO).collect(Collectors.toList());
    }

    @Override
    public SessionOutputDTO updateStatus(Long sessionId, SessionStatusUpdateInputDTO statusUpdateInputDTO) {

        final long[] delay = {statusUpdateInputDTO.getDelayInMinutes()};

        return repository.findById(sessionId).map(session -> {
            delay[0] = delay[0] - session.getDelayInMinutes();

            Session sessionToUpdate = session.toBuilder().status(statusUpdateInputDTO.getStatus())
                    .delayInMinutes(statusUpdateInputDTO.getDelayInMinutes())
                    .start(session.getStart().plusMinutes(delay[0])).end(session.getEnd().plusMinutes(delay[0]))
                    .build();

            sessionToUpdate = repository.save(sessionToUpdate);
            sendSmsAlert(sessionToUpdate);
            return sessionToUpdate.viewAsDTO();

        }).orElseThrow(() -> new ResourceNotFoundException("Session not found for id=" + sessionId));
    }

    @Override
    public SessionOutputDTO update(Long sessionId, SessionInputDTO sessionInput) {

        final long[] delay = {sessionInput.getDelayInMinutes()};

        Doctor doctor = doctorService.findEntityById(sessionInput.getDoctorId());
        Room room = roomService.findEntityById(sessionInput.getRoomId());

        return repository.findById(sessionId).map(p -> {
            delay[0] = delay[0] - p.getDelayInMinutes();

            Session session = p.toBuilder().doctor(doctor).room(room).id(sessionId).date(sessionInput.getDate())
                    .start(p.getStart().plusMinutes(delay[0])).end(p.getEnd().plusMinutes(delay[0]))
                    .status(sessionInput.getStatus()).note(sessionInput.getNote())
                    .delayInMinutes(sessionInput.getDelayInMinutes()).build();
            session = repository.save(session);
            sendSmsAlert(session);
            return session.viewAsDTO();
        }).orElseThrow(() -> new ResourceNotFoundException("Session not found for id=" + sessionId));
    }

    private void sendSmsAlert(Session session) {
        BulkSMSDTO bulkSMSDTO = null;

        if (session.getStatus().equals(SessionStatus.CANCELLED)) {
            bulkSMSDTO = smsGenerator.generateSMS(session, SMSTypes.CANCELLED);
        } else if (session.getStatus().equals(SessionStatus.DELAYED)) {
            bulkSMSDTO = smsGenerator.generateSMS(session, SMSTypes.DELAYED);
        } else if (session.getStatus().equals(SessionStatus.STARTED)) {
            bulkSMSDTO = smsGenerator.generateSMS(session, SMSTypes.STARTED);
        }

        if (!Objects.isNull(bulkSMSDTO)) {
			this.smsSender.send(bulkSMSDTO);
		}
    }

    @Override
    public Session findEntityById(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Session not found for id=" + id));

    }
}
