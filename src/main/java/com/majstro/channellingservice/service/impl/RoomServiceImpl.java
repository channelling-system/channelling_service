package com.majstro.channellingservice.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.majstro.channellingservice.dto.input.RoomInputDTO;
import com.majstro.channellingservice.dto.output.RoomOutputDTO;
import com.majstro.channellingservice.exception.ResourceNotFoundException;
import com.majstro.channellingservice.model.Room;
import com.majstro.channellingservice.repository.RoomRepository;
import com.majstro.channellingservice.service.RoomService;
import com.majstro.channellingservice.service.RoomServiceInternal;

@Service
public class RoomServiceImpl implements RoomService, RoomServiceInternal {

	private RoomRepository repository;

	public RoomServiceImpl(RoomRepository repository) {
		this.repository = repository;
	}

	@Override
	public RoomOutputDTO save(RoomInputDTO roomInput) {
		Room room = Room.builder().name(roomInput.getName()).description(roomInput.getDescription()).build();

		return repository.save(room).viewAsDTO();
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public RoomOutputDTO findById(Long id) {

		return repository.findById(id).map(p -> {
			return p.viewAsDTO();
		}).orElseThrow(() -> new ResourceNotFoundException("Room not found for id=" + id));
	}

	@Override
	public List<RoomOutputDTO> findAll() {
		return repository.findAll().stream().map(Room::viewAsDTO).collect(Collectors.toList());
	}

	@Override
	public RoomOutputDTO update(Long roomId, RoomInputDTO roomInput) {
		return repository.findById(roomId).map(p -> {
			Room room = Room.builder().id(roomId).name(roomInput.getName()).description(roomInput.getDescription())
					.sessionList(p.getSessionList()).build();
			return repository.save(room).viewAsDTO();
		}).orElseThrow(() -> new ResourceNotFoundException("Room not found for id=" + roomId));
	}

	@Override
	public Room findEntityById(Long id) {
		return repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Room not found for id=" + id));

	}
}
