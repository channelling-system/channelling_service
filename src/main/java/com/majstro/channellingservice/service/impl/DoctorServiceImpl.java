package com.majstro.channellingservice.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.majstro.channellingservice.dto.input.DoctorInputDTO;
import com.majstro.channellingservice.dto.output.DoctorOutputDTO;
import com.majstro.channellingservice.exception.ResourceNotFoundException;
import com.majstro.channellingservice.model.Doctor;
import com.majstro.channellingservice.repository.DoctorRepository;
import com.majstro.channellingservice.service.DoctorService;
import com.majstro.channellingservice.service.DoctorServiceInternal;

@Service
public class DoctorServiceImpl implements DoctorService, DoctorServiceInternal {

	private DoctorRepository repository;

	public DoctorServiceImpl(DoctorRepository repository) {
		this.repository = repository;
	}

	@Override
	public DoctorOutputDTO save(DoctorInputDTO doctorInput) {
		Doctor doctorToSave = Doctor.builder().name(doctorInput.getName()).telephone(doctorInput.getTelephone())
				.speciality(doctorInput.getSpeciality()).email(doctorInput.getEmail()).fee(doctorInput.getFee()).consultationTime(doctorInput.getConsultationTime()).build();

		return repository.save(doctorToSave).viewAsDTO();
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public DoctorOutputDTO findById(Long id) {

		return repository.findById(id).map(p -> {
			return p.viewAsDTO();
		}).orElseThrow(() -> new ResourceNotFoundException("Doctor not found for id=" + id));
	}
	
	@Override
	public Doctor findEntityById(Long id) {

		return repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Doctor not found for id=" + id));
	}

	@Override
	public List<DoctorOutputDTO> findAll() {
		return repository.findAll().stream().map(Doctor::viewAsDTO).collect(Collectors.toList());
	}

	@Override
	public DoctorOutputDTO update(Long doctorId, DoctorInputDTO doctorInput) {
		return repository.findById(doctorId).map(p -> {
			Doctor doctor = Doctor.builder().id(doctorId).name(doctorInput.getName()).telephone(doctorInput.getTelephone())
					.email(doctorInput.getEmail()).speciality(doctorInput.getSpeciality()).sessionList(p.getSessionList())
					.consultationTime(doctorInput.getConsultationTime()).fee(doctorInput.getFee()).build();
			return repository.save(doctor).viewAsDTO();
		}).orElseThrow(() -> new ResourceNotFoundException("Doctor not found for id=" + doctorId));
	}
}
