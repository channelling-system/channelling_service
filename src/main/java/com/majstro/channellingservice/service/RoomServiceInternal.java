package com.majstro.channellingservice.service;

import com.majstro.channellingservice.model.Room;

public interface RoomServiceInternal {

	Room findEntityById(Long id);

}
