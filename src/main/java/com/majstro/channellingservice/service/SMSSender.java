package com.majstro.channellingservice.service;

import com.majstro.channellingservice.dto.BulkSMSDTO;

public interface SMSSender {
	void send(BulkSMSDTO smses);
}
