package com.majstro.channellingservice.service;

import com.majstro.channellingservice.model.Doctor;

public interface DoctorServiceInternal {

	Doctor findEntityById(Long id);

}
