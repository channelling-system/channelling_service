package com.majstro.channellingservice.service;

import com.majstro.channellingservice.model.Session;

public interface SessionServiceInternal {

	Session findEntityById(Long id);

}
