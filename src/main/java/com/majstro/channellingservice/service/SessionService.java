package com.majstro.channellingservice.service;

import java.util.List;

import com.majstro.channellingservice.dto.input.SessionInputDTO;
import com.majstro.channellingservice.dto.input.SessionStatusUpdateInputDTO;
import com.majstro.channellingservice.dto.output.SessionOutputDTO;

public interface SessionService {

	SessionOutputDTO save( SessionInputDTO sessionInput);
	
	SessionOutputDTO update(Long sessionId,SessionInputDTO sessionInput);

	void deleteById(Long id);

	SessionOutputDTO findById(Long id);

	List<SessionOutputDTO> findAll();

	SessionOutputDTO updateStatus(Long sessionId, SessionStatusUpdateInputDTO statusUpdateInputDTO);

}
