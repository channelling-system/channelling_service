package com.majstro.channellingservice.service;

import java.util.List;

import com.majstro.channellingservice.dto.input.RoomInputDTO;
import com.majstro.channellingservice.dto.output.RoomOutputDTO;

public interface RoomService {

	RoomOutputDTO save(RoomInputDTO roomInput);
	
	RoomOutputDTO update(Long doctorId,RoomInputDTO roomInput);

	void deleteById(Long id);

	RoomOutputDTO findById(Long id);

	List<RoomOutputDTO> findAll();
}
