package com.majstro.channellingservice.dto.input;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class RoomInputDTO {
	private final String name;
	private final String description;

}
