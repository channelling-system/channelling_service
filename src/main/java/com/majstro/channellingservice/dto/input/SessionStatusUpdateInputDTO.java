package com.majstro.channellingservice.dto.input;

import com.majstro.channellingservice.model.SessionStatus;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class SessionStatusUpdateInputDTO {

	private final SessionStatus status;
	private final long delayInMinutes;

}
