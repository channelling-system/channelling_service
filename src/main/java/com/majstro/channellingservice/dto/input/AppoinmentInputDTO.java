package com.majstro.channellingservice.dto.input;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class AppoinmentInputDTO {

	private patientInputDTO patient;
	private Long sessionId;
	private boolean confirmed;
	private String note;
}
