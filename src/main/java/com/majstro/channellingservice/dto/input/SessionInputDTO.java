package com.majstro.channellingservice.dto.input;

import java.time.LocalDate;
import java.time.LocalTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.majstro.channellingservice.model.SessionStatus;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class SessionInputDTO {

	private final Long doctorId;
	private final Long roomId;
	private final long delayInMinutes;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private final LocalDate date;

	@JsonFormat(pattern = "hh:mm")
	private final LocalTime start;

	@JsonFormat(pattern = "hh:mm")
	private final LocalTime end;

	private final SessionStatus status;
	private final String note;
}
