package com.majstro.channellingservice.dto.input;

import com.majstro.channellingservice.model.Gender;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@Getter
@RequiredArgsConstructor
public class patientInputDTO {

	private final Long id;
	private final String name;
	private final String address;
	private final Gender gender;
	private final String telephone;
	private final LocalDate dob;
	private final int age;
	private final String nic;

}
