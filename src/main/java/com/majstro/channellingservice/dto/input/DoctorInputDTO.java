package com.majstro.channellingservice.dto.input;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class DoctorInputDTO {
	private String name;
	private String telephone;
	private String email;
	private double fee;
	private int consultationTime;
	private String speciality;
}
