package com.majstro.channellingservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public class SMSDTO {
	private final String number;
	private final String mask;
	private final String campaignName;
	private final String text;

	@JsonCreator
	public SMSDTO(@JsonProperty("number") String number, @JsonProperty("mask") String mask,
			@JsonProperty("campaignName") String campaignName, @JsonProperty("text") String text) {
		super();
		this.number = number;
		this.mask = mask;
		this.campaignName = campaignName;
		this.text = text;
	}
}
