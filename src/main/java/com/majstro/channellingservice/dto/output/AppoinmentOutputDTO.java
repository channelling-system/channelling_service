package com.majstro.channellingservice.dto.output;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class AppoinmentOutputDTO {
	private final long id;
	private final PatientOutputDTO patient;
	private final SessionOutputDTO session;
	private final Double doctorFee;
	private final Double hospitalFee;
	private final LocalDateTime time;
	private final int number;
	private final boolean confirmed;
	private final String note;
}