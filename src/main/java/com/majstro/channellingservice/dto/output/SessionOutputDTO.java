package com.majstro.channellingservice.dto.output;

import java.time.LocalDate;
import java.time.LocalTime;

import com.majstro.channellingservice.model.SessionStatus;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class SessionOutputDTO {
	
	private final long id;
	private final DoctorOutputDTO doctor;
	private final RoomOutputDTO room;
	private final LocalDate date;
	private final LocalTime start;
	private final LocalTime end;
	private final String note;
	private final SessionStatus status;
	private final double doctorFee;
	private final double hospitalFee;
	private final long appointmentCount;
	private final long confirmedAppointmentCount;
	private final long delayInMinutes;
}
