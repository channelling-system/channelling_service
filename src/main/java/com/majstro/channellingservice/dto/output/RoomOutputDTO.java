package com.majstro.channellingservice.dto.output;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class RoomOutputDTO {
	private final long id;
	private final String name;
	private final String description;

}
