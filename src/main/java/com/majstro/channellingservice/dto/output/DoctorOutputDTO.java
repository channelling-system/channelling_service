package com.majstro.channellingservice.dto.output;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class DoctorOutputDTO {
	private final long id;
	private final String name;
	private final String telephone;
	private final String email;
	private final double fee;
	private final int consultationTime;
	private final String speciality;
}