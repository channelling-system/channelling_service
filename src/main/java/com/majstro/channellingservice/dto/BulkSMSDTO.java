package com.majstro.channellingservice.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public class BulkSMSDTO {
	private final List<SMSDTO> messages;

	@JsonCreator
	public BulkSMSDTO(@JsonProperty("messages") List<SMSDTO> messages) {
		super();
		this.messages = messages;
	}
}
