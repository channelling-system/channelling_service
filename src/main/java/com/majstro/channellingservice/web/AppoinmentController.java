package com.majstro.channellingservice.web;

import java.util.List;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.majstro.channellingservice.dto.input.AppoinmentInputDTO;
import com.majstro.channellingservice.dto.output.AppoinmentOutputDTO;
import com.majstro.channellingservice.service.AppoinmentService;

@RestController
@SecurityRequirement(name = "bearer-key")
public class AppoinmentController {

	private AppoinmentService appoinmentService;

	public AppoinmentController(AppoinmentService appoinmentService) {
		this.appoinmentService = appoinmentService;
	}

	@PostMapping("/appoinments")
	public ResponseEntity<AppoinmentOutputDTO> createAppoinment(@RequestBody AppoinmentInputDTO appoinmentInput) {
		return new ResponseEntity<>(appoinmentService.save(appoinmentInput), HttpStatus.CREATED);

	}

	@GetMapping("/appoinments/{appoinmentId}")
	public ResponseEntity<AppoinmentOutputDTO> getAppoinment(@PathVariable(value = "appoinmentId") Long appoinmentId) {

		return new ResponseEntity<>(appoinmentService.findById(appoinmentId), HttpStatus.OK);

	}

	@GetMapping("/appoinments")
	public List<AppoinmentOutputDTO> getAppoinmentList() {
		return appoinmentService.findAll();
	}

	@GetMapping("/appoinments/session/{id}")
	public List<AppoinmentOutputDTO> getAppointmentListBySessionId(@PathVariable long id) {
		return appoinmentService.findAllBySessionId(id);
	}

	@PutMapping("/appoinments/{appoinmentId}")
	public AppoinmentOutputDTO updateAppoinment(@PathVariable(value = "appoinmentId") Long appoinmentId,
			@RequestBody AppoinmentInputDTO appoinmentInput) {
		return appoinmentService.update(appoinmentId, appoinmentInput);
	}

}
