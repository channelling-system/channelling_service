package com.majstro.channellingservice.web;

import com.majstro.channellingservice.auth.AuthService;
import com.majstro.channellingservice.auth.model.AuthenticationRequest;
import com.majstro.channellingservice.auth.model.AuthenticationResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

	private AuthService authService;

	public AuthController(AuthService authService) {
		this.authService = authService;
	}

	@PostMapping("/login")
	public ResponseEntity<AuthenticationResponse> login(@RequestBody AuthenticationRequest authenticationRequest) {
		return ResponseEntity.ok(authService.authenticate(authenticationRequest));
	}

}
