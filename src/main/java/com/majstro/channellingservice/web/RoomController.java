package com.majstro.channellingservice.web;

import java.util.List;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.majstro.channellingservice.dto.input.RoomInputDTO;
import com.majstro.channellingservice.dto.output.RoomOutputDTO;
import com.majstro.channellingservice.service.RoomService;

@RestController
@SecurityRequirement(name = "bearer-key")
public class RoomController {

	private RoomService roomService;

	public RoomController(RoomService roomService) {
		this.roomService = roomService;
	}

	@Operation(security = { @SecurityRequirement(name = "bearer-key") })
	@PostMapping("/rooms")
	public ResponseEntity<RoomOutputDTO> createRoom(@RequestBody RoomInputDTO roomInput) {
		return new ResponseEntity<>(roomService.save(roomInput), HttpStatus.CREATED);

	}

	@GetMapping("/rooms/{roomId}")
	public ResponseEntity<RoomOutputDTO> getRoom(@PathVariable(value = "roomId") Long roomId) {

		return new ResponseEntity<>(roomService.findById(roomId), HttpStatus.OK);

	}

	@GetMapping("/rooms")
	public List<RoomOutputDTO> getRoomList() {
		return roomService.findAll();
	}

	@PutMapping("/rooms/{roomId}")
	public RoomOutputDTO updateRoom(@PathVariable(value = "roomId") Long roomId, @RequestBody RoomInputDTO roomInput) {
		return roomService.update(roomId, roomInput);
	}

}
