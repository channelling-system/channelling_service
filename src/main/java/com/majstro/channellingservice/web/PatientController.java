package com.majstro.channellingservice.web;

import java.util.List;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.majstro.channellingservice.dto.input.patientInputDTO;
import com.majstro.channellingservice.dto.output.PatientOutputDTO;
import com.majstro.channellingservice.service.PatientService;

@RestController
@SecurityRequirement(name = "bearer-key")
public class PatientController {

	private PatientService patientService;

	public PatientController(PatientService patientService) {
		this.patientService = patientService;
	}

	@PostMapping("/patients")
	public ResponseEntity<PatientOutputDTO> createPatient(@RequestBody patientInputDTO patientInput) {
		return new ResponseEntity<>(patientService.save(patientInput), HttpStatus.CREATED);

	}

	@GetMapping("/patients/{patientId}")
	public ResponseEntity<PatientOutputDTO> getPatient(@PathVariable(value = "patientId") Long patientId) {

		return new ResponseEntity<>(patientService.findById(patientId), HttpStatus.OK);

	}

	@GetMapping("/patients")
	public List<PatientOutputDTO> getPatientList() {
		return patientService.findAll();
	}

	@PutMapping("/patients/{patientId}")
	public PatientOutputDTO updatePatient(@PathVariable(value = "patientId") Long patientId,
			@RequestBody patientInputDTO patientInput) {
		return patientService.update(patientId, patientInput);
	}

}
