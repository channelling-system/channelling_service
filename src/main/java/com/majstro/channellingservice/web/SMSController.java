package com.majstro.channellingservice.web;

import java.util.List;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.majstro.channellingservice.dto.BulkSMSDTO;
import com.majstro.channellingservice.dto.SMSDTO;
import com.majstro.channellingservice.service.SMSSender;

@RestController
@SecurityRequirement(name = "bearer-key")
@RequestMapping("/api/v1/sms")
public class SMSController {

	private final SMSSender smsSender;
	private final String mask;
	private final String campaignName;

	public SMSController(final SMSSender smsSender, @Value("${sms.auth.mask}") final String mask,
			@Value("${sms.auth.campaignname}") final String campaignName) {
		super();
		this.smsSender = smsSender;
		this.campaignName = campaignName;
		this.mask = mask;
	}

	@GetMapping("/health")
	public ResponseEntity<Void> checkSMSStatus() {
		final SMSDTO sms = new SMSDTO("94778507225", mask, campaignName, "Health Check Message !!!");
		this.smsSender.send(new BulkSMSDTO(List.of(sms)));
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
