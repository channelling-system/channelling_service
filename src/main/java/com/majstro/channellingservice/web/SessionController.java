package com.majstro.channellingservice.web;

import java.util.List;

import com.majstro.channellingservice.dto.input.SessionStatusUpdateInputDTO;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.majstro.channellingservice.dto.input.SessionInputDTO;
import com.majstro.channellingservice.dto.output.SessionOutputDTO;
import com.majstro.channellingservice.service.SessionService;

@RestController
@SecurityRequirement(name = "bearer-key")
public class SessionController {

	private SessionService sessionService;

	public SessionController(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	@PostMapping("/sessions")
	public ResponseEntity<SessionOutputDTO> createSession(@RequestBody SessionInputDTO sessionInput) {
		return new ResponseEntity<>(sessionService.save(sessionInput), HttpStatus.CREATED);

	}

	@GetMapping("/sessions/{sessionId}")
	public ResponseEntity<SessionOutputDTO> getSession(@PathVariable(value = "sessionId") Long sessionId) {

		return new ResponseEntity<>(sessionService.findById(sessionId), HttpStatus.OK);

	}

	@GetMapping("/sessions")
	public List<SessionOutputDTO> getSessionList() {
		return sessionService.findAll();
	}

	@PutMapping("/sessions/{sessionId}")
	public SessionOutputDTO updateSession(@PathVariable(value = "sessionId") Long sessionId,
			@RequestBody SessionInputDTO sessionInput) {
		return sessionService.update(sessionId, sessionInput);
	}

	@PatchMapping("/sessions/{sessionId}")
	public SessionOutputDTO updateSessionStatus(@PathVariable(value = "sessionId") Long sessionId,
									@RequestBody SessionStatusUpdateInputDTO updateInputDTO) {
		return sessionService.updateStatus(sessionId, updateInputDTO);
	}

}
