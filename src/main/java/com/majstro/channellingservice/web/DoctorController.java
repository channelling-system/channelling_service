package com.majstro.channellingservice.web;

import java.util.List;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.majstro.channellingservice.dto.input.DoctorInputDTO;
import com.majstro.channellingservice.dto.output.DoctorOutputDTO;
import com.majstro.channellingservice.service.DoctorService;

@RestController
@SecurityRequirement(name = "bearer-key")
public class DoctorController {

	private DoctorService doctorService;

	public DoctorController(DoctorService doctorService) {
		this.doctorService = doctorService;
	}

	@PostMapping("/doctors")
	public ResponseEntity<DoctorOutputDTO> createDoctor(@RequestBody DoctorInputDTO doctorInput) {
		return new ResponseEntity<>(doctorService.save(doctorInput), HttpStatus.CREATED);

	}

	@GetMapping("/doctors/{doctorId}")
	public ResponseEntity<DoctorOutputDTO> getDoctor(@PathVariable(value = "doctorId") Long doctorId) {

		return new ResponseEntity<>(doctorService.findById(doctorId), HttpStatus.OK);

	}

	@GetMapping("/doctors")
	public List<DoctorOutputDTO> getDoctorList() {
		return doctorService.findAll();
	}

	@PutMapping("/doctors/{doctorId}")
	public DoctorOutputDTO updateDoctor(@PathVariable(value = "doctorId") Long doctorId,
			@RequestBody DoctorInputDTO doctorInput) {
		return doctorService.update(doctorId, doctorInput);
	}

}
