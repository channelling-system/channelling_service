package com.majstro.channellingservice.model;

public enum SessionStatus {
NOT_STARTED, STARTED, DELAYED, COMPLETED, CANCELLED, HELD}
