package com.majstro.channellingservice.model;

public enum Gender {

	MALE, FEMALE;
}
