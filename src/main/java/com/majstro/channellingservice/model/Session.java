package com.majstro.channellingservice.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.majstro.channellingservice.dto.output.SessionOutputDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
@Entity
@Table(name = "sessions")
public class Session extends BaseEntity {

	@ManyToOne(fetch = FetchType.LAZY)
	private Doctor doctor;

	@ManyToOne(fetch = FetchType.LAZY)
	private Room room;

	@OneToMany(mappedBy = "session", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Appoinment> appoinmentList;

	private LocalDate date;
	private LocalTime start;
	private LocalTime end;
	
	@Enumerated(EnumType.STRING)
	private SessionStatus status; 
	
	private String note;
	private  double doctorFee;
	private  double hospitalFee;
	private  long delayInMinutes;

    @JsonIgnore
    public SessionOutputDTO viewAsDTO() {
        return new SessionOutputDTO(id, doctor.viewAsDTO(), room.viewAsDTO(), date, start, end, note, status,
                doctorFee, hospitalFee, !Objects.isNull(appoinmentList) ? appoinmentList.size() : 0,
                !Objects.isNull(appoinmentList) ? appoinmentList.stream()
                        .filter(Appoinment::isConfirmed).count() : 0, delayInMinutes);
    }
}
