package com.majstro.channellingservice.model;

public enum SMSTypes {
STARTED, DELAYED,  CANCELLED, CONFIRMED, NOT_CONFIRMED
}
