package com.majstro.channellingservice.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.majstro.channellingservice.dto.output.DoctorOutputDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "doctors")
public class Doctor extends BaseEntity {

	private String name;
	private String telephone;
	private String email;
	private double fee;
	private int consultationTime;

	private String speciality;

	@OneToMany(mappedBy = "doctor", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Session> sessionList;

	@JsonIgnore
	public DoctorOutputDTO viewAsDTO() {
		return new DoctorOutputDTO(id, name, telephone, email, fee, consultationTime, speciality);
	}
}
