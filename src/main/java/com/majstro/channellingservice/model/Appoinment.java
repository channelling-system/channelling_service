package com.majstro.channellingservice.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.majstro.channellingservice.dto.output.AppoinmentOutputDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "appoinments")
public class Appoinment extends BaseEntity {

	@ManyToOne(fetch = FetchType.LAZY)
	private Patient patient;

	@ManyToOne(fetch = FetchType.LAZY)
	private Session session;

	private Double doctorFee;
	private Double hospitalFee;
	private LocalDateTime time;
	private int number;
	private boolean confirmed;
	private String note;

	@JsonIgnore
	public AppoinmentOutputDTO viewAsDTO() {
		return new AppoinmentOutputDTO(id, patient.viewAsDTO(), session.viewAsDTO(), doctorFee, hospitalFee, time,
				number, confirmed, note);
	}
}
