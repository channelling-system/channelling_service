package com.majstro.channellingservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.majstro.channellingservice.dto.output.PatientOutputDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.List;

@Getter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "patients")
public class Patient extends BaseEntity {

	private String name;
	private String address;
	
	@Enumerated(EnumType.STRING)
	private Gender gender;
	
	private String telephone;
	private LocalDate dob;
	private int age;
	private String nic;

	@OneToMany(mappedBy = "patient", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Appoinment> appoinmentList;

	@JsonIgnore
	public PatientOutputDTO viewAsDTO() {
		return new PatientOutputDTO(id, name, address, gender, telephone, dob, age, nic);
	}
}
