package com.majstro.channellingservice.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.majstro.channellingservice.dto.output.RoomOutputDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor
@SuperBuilder
@Entity
@Table(name = "rooms")
public class Room extends BaseEntity {

	private String name;
	private  String description;
	
	@OneToMany(mappedBy = "room", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Session> sessionList;

	@JsonIgnore
	public RoomOutputDTO viewAsDTO() {
		return new RoomOutputDTO(id, name, description);
	}
}
