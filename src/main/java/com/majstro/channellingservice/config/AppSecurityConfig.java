package com.majstro.channellingservice.config;

import com.majstro.channellingservice.auth.JwtRequestFilter;
import com.majstro.channellingservice.service.impl.UserDetailsServiceImpl;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Configuration
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtRequestFilter jwtRequestFilter;
    private final String ldapUrl;
    private final String ldapBaseDn;
    private final String ldapUserDnPattern;

    public AppSecurityConfig(JwtRequestFilter jwtRequestFilter,
                             @Value("${spring.ldap.urls}") String ldapUrl,
                             @Value("${ldap.base.dn}") String ldapBaseDn,
                             @Value("${ldap.user.dn.pattern}") String ldapUserDnPattern) {

        this.jwtRequestFilter = jwtRequestFilter;
        this.ldapUrl = ldapUrl;
        this.ldapBaseDn = ldapBaseDn;
        this.ldapUserDnPattern = ldapUserDnPattern;

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().exceptionHandling()
                .and()
                .authorizeRequests()
                .antMatchers("/login", "/swagger-ui/**", "/swagger-ui.html","/v3/api-docs/**").permitAll()
                .antMatchers("/actuator/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .ldapAuthentication()
                .userDnPatterns(ldapUserDnPattern)
                .contextSource()
                .url(ldapUrl + "/" + ldapBaseDn)
                .and()
                .passwordCompare()
                .passwordAttribute("userPassword");
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsServiceImpl();
    }

    @Bean
    public AuthenticationManager anAuthenticationManager() throws Exception {
        return authenticationManager();
    }

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .components(new Components()
                        .addSecuritySchemes("bearer-key",
                                new SecurityScheme().type(SecurityScheme.Type.HTTP)
                                        .scheme("bearer")
                                        .bearerFormat("JWT")));
    }

}
