#!/usr/bin/env bash
cd /home/ec2-user

echo  "Login to Docker Hub"
docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD

echo "Starting deployment"

if [ "$DEPLOYMENT_GROUP_NAME" == "PROD" ]
then
 echo "Deploying to PROD env"
 sudo docker run --network="host" -t --name channeling-service amsamila/channeling-service:1.0.12 > application.log  2>&1 &
# exit 0
fi
