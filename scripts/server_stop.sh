#!/usr/bin/env bash
cd /home/ec2-user

echo "Stopping PROD"
sudo docker stop channeling-service
sudo docker rm channeling-service
sudo rm -fr application.log