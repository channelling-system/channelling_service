FROM adoptopenjdk/openjdk12:latest
VOLUME /tmp
EXPOSE 80
ADD target/chanelling-service-0.0.1-SNAPSHOT.jar channeling-service.jar
ENTRYPOINT ["java","-jar","channeling-service.jar"]